'use strict';

import quiz from './quiz.json' assert { type: 'json' };

/** ##  LocalStorage  ## */
localStorage.setItem('quiz', JSON.stringify(quiz));
const localStorageQuiz = JSON.parse(localStorage.getItem('quiz'));

// creamos los items si no existen
if (!localStorage.getItem('nQuestion')) {
  localStorage.setItem('nQuestion', 0);
}
if (!localStorage.getItem('aTrue')) {
  localStorage.setItem('aTrue', 0);
}

// STATE
let state = { quiz: localStorageQuiz || [] };

function randomState() {
  localStorageQuiz.sort((a, b) => 0.5 - Math.random());
  state = { quiz: localStorageQuiz || [] };
  reset();
}

// Declaramos variables
let questionH1 = document.getElementById('question');
let answersUL = document.getElementById('answers');
let buttonReset = document.getElementById('reset');
let questionNumber = document.getElementById('question-number');

// FUNCION DE RESETEO PARA "NQUESTION" Y "ATRUE"

function setStorage(item) {
  let num = Number(localStorage.getItem(item)) + 1;
  localStorage.setItem(item, num);
}

// FUNCION PARA EL CLICK DE LOS "LI"

function eventoClick(e, correct) {
  if (correct === e.target.innerText) {
    setStorage('aTrue');
  }
  setStorage('nQuestion');
  render();
}
// FUNCION PARA CREAR LOS "UL" Y "LI"

function createLI(answers, correct) {
  answersUL.textContent = '';
  const UL = document.createElement('ul');
  for (let i = 0; i < answers.length; i++) {
    const LI = document.createElement('li');
    LI.classList.add('item');
    LI.innerHTML = answers[i];
    UL.append(LI);
    LI.addEventListener('click', (e) => eventoClick(e, correct));
  }
  answersUL.append(UL);
}

// RENDER

function render() {
  questionH1.textContent = '';
  const H1 = document.createElement('h1');
  let a = localStorage.getItem('nQuestion');
  if (a <= 49) {
    let { question, answers, correct } = state.quiz[a];
    H1.innerHTML = question;
    createLI(answers, correct);
    questionNumber.innerText = ++a;
    questionNumber.textContent += '/50';
  } else {
    questionNumber.textContent = '';
    answersUL.textContent = '';
    H1.innerHTML = 'Puntuación Final:' + localStorage.getItem('aTrue');
  }
  questionH1.append(H1);
}
render();

// FUNCION BOTON RESETEO

function reset() {
  localStorage.setItem('aTrue', 0);
  localStorage.setItem('nQuestion', 0);
  render();
}
buttonReset.addEventListener('click', reset);
document.getElementById('aleatorio').addEventListener('click', randomState);
